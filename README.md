# WiPro Test

Work and consumption with REST API from OpenWeatherMap.

Libraries used:
*  LiveData -   LiveData is used for interaction between ViewModel and View.
*  Retrofit -   Retrofit is used for network traffic.
*  RxJava2  -   RxJava is use for threading and transformations.
*  JodaTime -   Used for DateTime parsing and formatting.
*  Dagger2  -   Dagger is used for Dependency injection.

Kotlin is used throughout.


## Future improvements

*  Unit tests for logic (almost no logic currently present)
*  Storing into Room and then observing in UI (This will allow always present data, which can then be updated on Activity start)
*  Better UI
  


### Issues
*  Faces issued with retriving Celcius data from API. Performing local Kelvin to Celcius conversations.
