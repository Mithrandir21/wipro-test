package com.example.testapp.di

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testapp.App
import com.example.testapp.api.ForecastApi
import com.example.testapp.api.RetrofitProvider
import com.example.testapp.di.annotations.AuthToken
import com.example.testapp.repo.ForecastRepo
import com.example.testapp.repo.ForecastRepoImpl
import com.example.testapp.ui.Main
import com.example.testapp.ui.MainViewModel
import com.example.testapp.viewmodel.ViewModelFactory
import com.example.testapp.viewmodel.ViewModelMapKey
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module(includes = [AndroidSupportInjectionModule::class, AppDependencyBinder::class, ViewModelDependencyProvider::class])
class AppDependencyProvider {

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideRetrofit(retrofitProvider: RetrofitProvider): Retrofit =
            retrofitProvider.getRetrofit()

    /** Provides [ForecastApi]. */
    @Provides
    fun provideCardsApi(retrofit: Retrofit): ForecastApi = retrofit.create(ForecastApi::class.java)

    @Provides
    @AuthToken
    fun provideApiAuthToken(): String = "3ae7da3a2f3377ba24c5bb8f1c478497"
}

/** An interface defining the way application-wide dependencies are being bound. */
@Module
interface AppDependencyBinder {

    /** Binds the [App] context to `Application`. */
    @Binds
    fun bindApplication(application: App): Application

    /** Binds the [App] context to `Context`. */
    @Binds
    fun bindContext(application: App): Context

    /** Binds an instance of [ViewModelFactory] to `ViewModelProvider.Factory`. */
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    /** Binds the [ForecastRepoImpl] context to [ForecastRepo]. */
    @Binds
    fun bindsForecastRepo(repo: ForecastRepoImpl): ForecastRepo
}