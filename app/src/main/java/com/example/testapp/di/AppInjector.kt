package com.example.testapp.di

import com.example.testapp.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppDependencyProvider::class
    ]
)
interface AppInjector {

    /** Injects dependencies into the [app]. */
    fun inject(app: App)

    /** An interface defining [AppInjector] building process. */
    @Component.Builder
    interface Builder {

        /** Defines the [app] instance to be used for dependency injection. */
        @BindsInstance
        fun application(app: App): Builder

        /** Builds an [AppInjector]. */
        fun build(): AppInjector
    }
}
