package com.example.testapp.di

import androidx.lifecycle.ViewModel
import com.example.testapp.ui.Main
import com.example.testapp.ui.MainViewModel
import com.example.testapp.viewmodel.ViewModelMapKey
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [AndroidInjectionModule::class])
interface ViewModelDependencyProvider {

    @ContributesAndroidInjector
    fun injectMainActivity(): Main

    @Binds
    @IntoMap
    @ViewModelMapKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}