package com.example.testapp.repo

import com.example.testapp.api.ForecastApi
import com.example.testapp.di.annotations.AuthToken
import com.example.testapp.domain.Forecast
import io.reactivex.Observable
import javax.inject.Inject

class ForecastRepoImpl @Inject constructor(val api: ForecastApi, @AuthToken val authToken: String) : ForecastRepo {
    override fun loadNewItems(): Observable<Forecast> = api.getItem("Edinburgh,UK", authToken).toObservable()
}