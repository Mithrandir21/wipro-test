package com.example.testapp.repo

import com.example.testapp.domain.Forecast
import io.reactivex.Observable

interface ForecastRepo {
    fun loadNewItems(): Observable<Forecast>
}