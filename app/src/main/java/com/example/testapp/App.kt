package com.example.testapp

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.example.testapp.di.DaggerAppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

    /** An `AndroidInjector` used to inject dependencies into Activities. */
    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    /** An `AndroidInjector` used to inject dependencies into Fragments. */
    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    /** Returns an `AndroidInjector` used to inject dependencies into Activities. */
    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    /** Returns an `AndroidInjector` used to inject dependencies into Fragments. */
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    /**
     * Called when the application is starting.
     *
     * Initializes Fabric and dependency injection.
     */
    override fun onCreate() {
        super.onCreate()

        DaggerAppInjector.builder()
            .application(this)
            .build()
            .inject(this)
    }
}
