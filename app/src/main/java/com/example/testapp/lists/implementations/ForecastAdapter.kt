package com.example.testapp.lists.implementations

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.testapp.R
import com.example.testapp.databinding.ListItemPeriodItemLayoutBinding
import com.example.testapp.domain.ForecastItem
import com.example.testapp.lists.BaseAdapterDelegate
import com.example.testapp.lists.BaseViewHolder


class ForecastAdapter : BaseAdapterDelegate<ForecastItem, BaseViewHolder<ListItemPeriodItemLayoutBinding>>(ForecastItem::class.java) {

    @SuppressLint("SetTextI18n")
    override fun bindViewHolder(item: ForecastItem, holder: BaseViewHolder<ListItemPeriodItemLayoutBinding>) {
        holder.bind {
            forecastTime.text = item.datetime
            forecastHumidity.text = "Hum: ${item.main.humidity}%"
            forecastRain.text = item.rain?.rain?.let { "Rain: $it mm" } ?: "No rain"
            forecastWind.text = "Wind ${item.wind.speed} m/s"
            forecastTemp.text = String.format("%.1f °C", item.main.tempMax.toFloat().minus(273.15f))
        }
    }

    override fun createViewHolder(parent: ViewGroup): BaseViewHolder<ListItemPeriodItemLayoutBinding> =
        BaseViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item_period_item_layout, parent, false))


    override fun getItemUniqueID(item: ForecastItem): Long = item.hashCode().toLong()
}