package com.example.testapp.lists

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PreFetchLayoutManager @JvmOverloads constructor(
    context: Context,
    orientation: Int = RecyclerView.VERTICAL,
    reverseLayout: Boolean = false,
    private val extraLayoutSpace: Int = DEFAULT_EXTRA_LAYOUT_SPACE
) : LinearLayoutManager(context, orientation, reverseLayout) {

    override fun getExtraLayoutSpace(state: RecyclerView.State): Int =
        if (extraLayoutSpace > 0) extraLayoutSpace else DEFAULT_EXTRA_LAYOUT_SPACE

    companion object {
        const val DEFAULT_EXTRA_LAYOUT_SPACE = 2000
    }
}