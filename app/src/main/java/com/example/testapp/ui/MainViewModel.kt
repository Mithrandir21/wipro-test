package com.example.testapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testapp.domain.Forecast
import com.example.testapp.repo.ForecastRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(private val forecastRepo: ForecastRepo) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val errors = MutableLiveData<Throwable>()
    private val forecast = MutableLiveData<Forecast>()

    fun observeErrors(): LiveData<Throwable> = errors
    fun observeForecast(): LiveData<Forecast> = forecast


    fun loadForecast() =
        forecastRepo.loadNewItems()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(forecast::postValue, errors::postValue)
            .let { compositeDisposable.add(it) }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}