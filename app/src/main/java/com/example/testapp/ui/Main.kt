package com.example.testapp.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.testapp.R
import com.example.testapp.lists.AdapterDelegatesManager
import com.example.testapp.lists.BaseAdapter
import com.example.testapp.lists.PreFetchLayoutManager
import com.example.testapp.lists.implementations.ForecastAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.main_layout.*
import javax.inject.Inject

class Main : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java) }

    private lateinit var adapter: BaseAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_layout)

        list.layoutManager = PreFetchLayoutManager(this)
        adapter = BaseAdapter(AdapterDelegatesManager().addDelegate(ForecastAdapter()))
        list.adapter = adapter

        viewModel.observeErrors().observe(this, Observer {
            Toast.makeText(this, "There was an error", Toast.LENGTH_SHORT).show()
            it.printStackTrace()
        })
        viewModel.observeForecast().observe(this, Observer {
            adapter.replaceData(it.list)
        })
    }

    override fun onStart() {
        super.onStart()

        viewModel.loadForecast()
    }
}