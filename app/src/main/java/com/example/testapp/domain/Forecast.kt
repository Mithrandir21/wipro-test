package com.example.testapp.domain

import com.google.gson.annotations.SerializedName

data class Forecast(val cod: String?, val message: Double?, val cnt: Int?, val list: List<ForecastItem> = listOf(), val city: City)

data class City(val id: Int, val name: String, val coord: Coordinates, val country: String)

data class Clouds(val all: Int? = 0)

data class Coordinates(val lat: Double?, val lon: Double?)

data class ForecastItem(
    val dt: Long,
    val main: Main,
    val weather: List<Weather>?,
    val clouds: Clouds?,
    val wind: Wind,
    val rain: Rain?,
    @SerializedName("dt_txt")
    val datetime: String)

data class Main(
    val temp: Double,
    @SerializedName("temp_min")
    val tempMin: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    val pressure: Double,
    @SerializedName("sea_level")
    val seaLevel: Double,
    @SerializedName("grnd_level")
    val grndLevel: Double,
    val humidity: Int,
    @SerializedName("temp_kf")
    val tempKf: Double)

data class Weather(val id: Int?, val main: String?, val description: String?, val icon: String?)

data class Wind(val speed: Double, val deg: Double?)

data class Rain(
    @SerializedName("3h")
    val rain: Double
)