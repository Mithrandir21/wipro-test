package com.example.testapp.api

import com.example.testapp.domain.Forecast
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/** REST API for Forecasts. */
interface ForecastApi {

    @GET("/data/2.5/forecast")
    fun getItem(@Query("q") cityQuery: String, @Query("appid") authToken: String): Single<Forecast>
}