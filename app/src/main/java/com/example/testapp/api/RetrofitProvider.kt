package com.example.testapp.api

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitProvider @Inject constructor(private val gson: Gson) {

    private var retrofit: Retrofit? = null

    /** Returns an instance of `Retrofit`. */
    fun getRetrofit(): Retrofit =
        retrofit ?: createRetrofit()


    private fun createRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl("https://samples.openweathermap.org/")
            .client(createHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()


    private fun createHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
}