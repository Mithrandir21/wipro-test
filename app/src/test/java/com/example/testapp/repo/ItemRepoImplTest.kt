package com.example.testapp.repo

import com.example.testapp.api.ForecastApi
import com.example.testapp.db.ItemsDao
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ItemRepoImplTest {

    @Mock
    private lateinit var itemsApi: ForecastApi

    @Mock
    private lateinit var itemsDao: ItemsDao


    @Test
    fun itemsShouldBeObserved() {
        // TODO - Test observing items being emitted
    }

    @Test
    fun newItemsShouldBeLoaded() {
        // TODO - Test new items being loaded from Source (Remote or otherwise)
    }
}